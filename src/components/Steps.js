import { Component } from "react";

class StepsComponent extends Component{
    render() {
        let {stepProps} = this.props
        return(
            <>
               {stepProps.map((element, index) => {
                return <ul key={index}>
                    <li >Bước: {element.id}</li>
                    <li >Title: {element.title}</li>
                    <li >Content: {element.content}</li>
                </ul>
               })}
           
            </>
        
        
        )
    }
}
export default StepsComponent