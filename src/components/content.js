import { Component } from "react";
import StepsComponent from "./Steps";

class ContentComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            steps: [
                { id: 1, title: 'Hello World', content: 'Welcome to learning React!' },
                { id: 2, title: 'Installation', content: 'You can install React from npm.' },
                { id: 3, title: 'Create react app', content: 'Run create-react-app to run project.' },
                { id: 4, title: 'Run init project', content: 'Cd into project and npm start to run project.' },
            ]
        }

    }


    render() {

        return (
            <StepsComponent stepProps={this.state.steps}></StepsComponent>
        )
    }
}
export default ContentComponent