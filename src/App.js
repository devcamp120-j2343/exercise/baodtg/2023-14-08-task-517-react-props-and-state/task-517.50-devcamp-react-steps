import './App.css';
import ContentComponent from './components/content';

function App() {
  return (
    <div >
     <ContentComponent></ContentComponent>
    </div>
  );
}

export default App;
